var http = require('http');
const database = require('./config/database');
const app = require('./config/express');

database('mongodb://localhost/leocc');
require('./models');

const resources = require('./resources');
resources(app);

const port = 3010;
http.createServer(app)
.listen(port, function(){
    console.log("Servidor iniciado", port)
})