// API de Atividades

const crud = require('../utils/crud');

module.exports = function (app) {
    crud(app, 'atividade');
}