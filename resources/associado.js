// API de Associados

const crud = require('../utils/crud');

module.exports = function (app) {
    crud(app, 'associado');
}