const mongoose = require('mongoose');

const atividadeSchema = new mongoose.Schema({
    titulo: {
        type: String,
        required: true
    },
    categoria: String,
    descricao: {
        type: String,
        required: true
    },
    data: Date
}, {timestamps: true});
const atividade = mongoose.model('atividade', atividadeSchema);

module.exports = atividade;
