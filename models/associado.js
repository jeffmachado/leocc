const mongoose = require('mongoose');

const associadoSchema = new mongoose.Schema({
    mylci: Number,
    nome: {
        type: String,
        required: true
    },
    sobrenome: {
        type: String,
        required: true
    },
    cargo: String,
    posse: Date,
    nascimento: Date,
    telefone: String,
    email: String,
    endereco: String,
    bairro: String,
    cidade: String,
    cep: String,
    ativo: Boolean
}, {timestamps: true});
const associado = mongoose.model('associado', associadoSchema);

module.exports = associado;
